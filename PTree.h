#ifndef PTREE_H
#define PTREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pizza.h"
#include <limits.h>

//Structure of B+ Tree of pizzas
typedef struct arvpiz{ 
    /* activKeys is the number of active keys
    * isLeaf is a boolean that says if the node is leaf
    * lastIndex if a boolean that says if it's children are data
    * keys is an array of pizza codes */
    int activKeys, isLeaf, lastIndex, *keys;

    // children is an array to seek the node's children
    // brother is a pointer to seek the node's brother
    // pizzas is an array to seek the node's pizzas
    long *children, *pizzas, brother; 

}PTree;


//Structure of descriptor
typedef struct desc{
    //To organize interation between files of indexes and data
    //indexes and datas stores the names of the index file and data file respectively
    char *indexes, *datas, *pizzas, *busca;

    // boolean to say if the root in the data file or not
    int rootInData;

    // pointer seek to where the root is in the file
    long root;

    // Ramification factor
    int t;

}Desc;


//PUBLIC METHODS
//PUBLIC METHODS
//PUBLIC METHODS

//Allocates memory for a Desc node
Desc * createDescriptor(int t);

//Make de inicial decisions for insertion
int inserts(Desc * descriptor, Pizza * pizza);

//Saves a node in the current posicion of the archives
void save_node(FILE * f, PTree * tree, int t);

//Try to read a node from de current posicion of the arquive. Returns NULL if it does not succeed;
PTree * read_node(FILE * f, int t);

//Control operations with the user and the Data Base
Desc * operateDB(char * entry, Desc * desc);

//returns the sizer of the structure PTree
int sizeOfPTree(int t);

//Try to open a file and if it can't be opened, prints a error message
FILE * openFile(const char * file, const char * type);

void searchCategory(Desc * desc, char * category);

void freePTree(PTree * pt);

long search_pizza(Desc * descriptor, int code);

void editPizza(Desc * desc, long pos_pizza);

//tests the first condicion and calls remove
void init_remove(Desc * descriptor, int code);

//removes a node from the tree
void removes(Desc * descriptor, PTree * node, int code, long pos_node);

void removeCategory ( Desc * desc);

long smallerNode(Desc * desc);

Desc * getDescriptor();

Desc * readDescriptor();

void writeDescriptor(Desc * desc);

int readBranchFactor();

int realSizeOfDesc();

Desc * eraseFiles(Desc * desc);

#endif //PTREE_H