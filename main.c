#include "PTree.h"
#include "pizza.h"
#include "interfaceuser.h"

//gcc interfaceuser.c pizza.c PTree.c main.c -o trabalho
//gcc interfaceuser.c pizza.c PTree.c main.c -o trabalho -DDEBUG=1

//gcc -static -g -o debugando interfaceuser.c pizza.c PTree.c main.c -DDEBUG=1
//valgrind --track-origins=yes ./debugando

int main () {

  char entry[20];
  readEntry(entry);
  Desc * descriptor = getDescriptor();
  writeDescriptor(operateDB(entry, descriptor));

  return 0;
}