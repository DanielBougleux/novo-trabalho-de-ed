#include <string.h>
#include <stdlib.h>
#include "pizza.h"

Pizza *pizza(int code, char *name, char *description, float price)
{
	Pizza *p = (Pizza *) malloc(sizeof(Pizza));
	if (p) memset(p, 0, sizeof(Pizza));
	p->code = code;
	strcpy(p->name, name);
	strcpy(p->description, description);
	p->price = price;
	return p;
}


void savePizza(Pizza *p, FILE *out)
{
	fwrite(&p->code, sizeof(int), 1, out);
	fwrite(p->name, sizeof(char), sizeof(p->name), out);
	fwrite(p->description, sizeof(char), sizeof(p->description), out);
	fwrite(&p->price, sizeof(float), 1, out);
}


Pizza *readPizza(FILE *in)
{
	Pizza *p = (Pizza *) malloc(sizeof(Pizza));
	if (0 >= fread(&p->code, sizeof(int), 1, in)) {
		free(p);
		return NULL;
	}
	fread(p->name, sizeof(char), sizeof(p->name), in);
	fread(p->description, sizeof(char), sizeof(p->description), in);
	fread(&p->price, sizeof(float), 1, in);
	return p;
}


int comparePizza(Pizza *p1, Pizza *p2)
{
	if (p1 == NULL) {
		return (p2 == NULL);
	}
	if (p1->code != p2->code) {
		return 0;
	}
	if (strcmp(p1->name, p2->name) != 0) {
		return 0;
	}
	if (strcmp(p1->description, p2->description) != 0) {
		return 0;
	}
	if (p1->price != p2->price) {
		return 0;
	}
	return 1;
}


int sizeOfPizza() //Retorna 78
{
	return sizeof(int) + // code (4)
		sizeof(char) * 50 + // name (50)
		sizeof(char) * 20 + // categoria (20)
		sizeof(float); // price (4)
}