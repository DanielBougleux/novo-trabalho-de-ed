#include "PTree.h"
#include "interfaceuser.h"

//PRIVATE METHODS
static PTree * create(int t);
static void splits(Desc * descriptor, long pos_c, 
        long pos_p ,int n, int pInData, int cInData, int pIsNewRoot);
static void insertNotFull(PTree * pt, Desc * desc, Pizza * p, long pos_pt);

#if DEBUG
static void printFiles(Desc * desc);
#endif

Desc * createDescriptor(int t){

    //Allocates memory for a Desc node
    Desc * novo = (Desc *)malloc(sizeof(Desc));
    //Guarantees that the memory block is filled with zero
    if(novo) memset(novo, 0, sizeof(Desc));

    novo->indexes = (char*)malloc(sizeof(char)*12);
    novo->datas = (char*)malloc(sizeof(char)*10);
    novo->pizzas = (char*)malloc(sizeof(char)*11);
    novo->busca = (char*)malloc(sizeof(char)*10);


    //Copies the parameters
    strcpy(novo->datas, "datas.dat");
    strcpy(novo->indexes, "indexes.dat");
    strcpy(novo->pizzas, "pizzas.dat");
    strcpy(novo->busca, "busca.dat");
    novo->t = t;

    FILE * data = openFile(novo->datas, "ab");
    FILE * index = openFile(novo->indexes, "ab");
    FILE * pizzas = openFile(novo->pizzas, "ab");
    FILE * busca = openFile(novo->busca, "wb");

    fclose(data);
    fclose(index);
    fclose(pizzas);
    fclose(busca);

    //sets the RootInData boolean to true
    novo->rootInData = 1;

    //Returns the node
    return novo;
}

static PTree * create(int t){

    //Allocates memory for the node
    PTree * novo = (PTree*)malloc(sizeof(PTree));

    //Guarantees that the memory block is filled with zero
    if(novo) memset(novo, 0, sizeof(PTree));

    //Allocates memory for the array of keys
    novo->keys = (int*)malloc(sizeof(int)*((2*t)-1));

    //sets every key to -1
    int i;
    for(i = 0; i<((2*t)-1); i++) novo->keys[i] = INT_MAX;

    //sets the brother position to -1, witch means that there's no brother
    novo->brother = LONG_MAX;

    //Allocates memory for the array of pointers to children
    novo->children = (long*)malloc(sizeof(long)*(2*t));

    //Sets every chindren to -1 by default
    for(i = 0; i<(2*t); i++) novo->children[i] = LONG_MAX;

    //Allocates memory for the array of seeks to pizza
    novo->pizzas = (long*)malloc(sizeof(long)*((2*t)-1));

    for(i = 0; i<((2*t)-1); i++) novo->pizzas[i] = LONG_MAX;

    //The new made node is set to be leaf
    novo->isLeaf = 1;

    //Returns the node
    return novo;
}

void save_node(FILE * f, PTree * tree, int t){

    if(fwrite(&tree->activKeys,sizeof(int), 1, f) != 1) {
        errorInWritingData();
        exit(1);
    }

    if(fwrite(&tree->isLeaf,sizeof(int), 1, f) != 1) {
        errorInWritingData();
        exit(1);
    }

    if(fwrite(&tree->lastIndex,sizeof(int), 1, f) != 1) {
        errorInWritingData();
        exit(1);
    }

    int i;
    for(i = 0; i<((2*t)-1);i++){
        if(fwrite(&tree->keys[i],sizeof(int), 1, f) != 1) {
            errorInWritingData();
            exit(1);
        }
    }

    for(i = 0;  i<(2*t); i++){
        if(fwrite(&tree->children[i],sizeof(long), 1, f) != 1) {
            errorInWritingData();
            exit(1);
        }
    }

    for(i = 0; i<((2*t)-1); i++){
        if(fwrite(&tree->pizzas[i],sizeof(long), 1, f) != 1) {
            errorInWritingData();
            exit(1);
        }
    }

    if(fwrite(&tree->brother,sizeof(long), 1, f) != 1) {
        errorInWritingData();
        exit(1);
    }

}

PTree * read_node(FILE * f, int t) {

    //Creates a node to hold the information read from the file
    PTree * novo = create(t);

    //Reads all the information in the file related to the PTree node
    fread(&novo->activKeys, sizeof(int), 1, f);
    fread(&novo->isLeaf, sizeof(int), 1, f);
    fread(&novo->lastIndex, sizeof(int), 1, f);
    int i;
    for(i = 0; i<((2*t)-1); i++){
        fread(&novo->keys[i], sizeof(int), 1, f);
    }

    for(i = 0; i<(2*t);i++){
        fread(&novo->children[i], sizeof(long), 1, f);
    }

    for(i = 0; i<((2*t)-1); i++){
        fread(&novo->pizzas[i], sizeof(long), 1, f);
    }

    fread(&novo->brother, sizeof(long), 1, f);

    //Returns the node with all the informations
    return novo;
}

int inserts(Desc * descriptor, Pizza * pizza){

    //search for the pizza in the tree, if it is already in the tree, do nothing
    if(search_pizza(descriptor, pizza->code) != LONG_MAX) {
        messagePizzaAlreadyExists();
        return 0;
    }
    
    //opens every necessary archive
    FILE * index = openFile(descriptor->indexes, "rb+");
    FILE * data = openFile(descriptor->datas, "rb+");
    FILE * pizza_f = openFile(descriptor->pizzas, "ab");

    long pos_pizza = ftell(pizza_f);

    //saves the pizza in the pizza file
    savePizza(pizza, pizza_f);

    //Assigns variables to check if the file is empty
    fseek(data, 0L, SEEK_END);
    long endOfFile = ftell(data);
    fseek(data, 0L, SEEK_SET);
    long startOfFile = ftell(data);

    //if the tree does not exist..
    if(descriptor->rootInData && (startOfFile == endOfFile)){

        //create a new node to become the root
        PTree * new = create(descriptor->t);

        //adds the new pizza's key
        new->keys[0] = pizza->code;

        //adds the new pizza position in archive
        new->pizzas[0] = pos_pizza;

        //the key we just inserted
        new->activKeys = 1;

        //save the root in the archive
        save_node(data,new,descriptor->t);

        //closes every archive and releases the node in memory
        fclose(index);
        fclose(data);
        fclose(pizza_f);
        freePTree(new);

        //return with success code
        return 1;

    }

    //if the root is already in the archive
    
    //reads the root from archive
    PTree * root;

    if(descriptor->rootInData){
        fseek(data, descriptor->root, SEEK_SET);
        root = read_node(data, descriptor->t);
    }else{
        fseek(index, descriptor->root, SEEK_SET);
        root = read_node(index, descriptor->t);
    }

    #if DEBUG
    printf("LI A RAIZ\n");
    #endif

    //if the root is full
    if(root->activKeys == ((descriptor->t*2)-1)){

        //creates a new root
        PTree * newroot = create(descriptor->t);

        //sets the boolean isleaf to 0 because root will be an index now
        newroot->isLeaf = 0;

        //New root is gonna be the last element in the index file
        if(root->isLeaf) newroot->lastIndex = 1;

        //the first child of the new root will be the last root
        newroot->children[0] = descriptor->root;
        

        //saves the new root at the last position of the archive
        fseek(index, 0L, SEEK_END);
        long pos_p = ftell(index);
        save_node(index, newroot, descriptor->t);

        //saves boolean for leaf in old root
        int leaf = root->isLeaf;

        //closes the archives that no more will be needed and the root that was saved
        fclose(data);
        fclose(pizza_f);
        fclose(index);
        freePTree(root);
        freePTree(newroot);

        //splits the last root and organizes the elements
        splits(descriptor, descriptor->root, pos_p, 1, 0, leaf, 1);

        index = openFile(descriptor->indexes, "rb");

        //reads the new root 
        fseek(index, descriptor->root, SEEK_SET);
        newroot = read_node(index, descriptor->t);

        //closes the index file
        fclose(index);

        //inserts the element in the tree
        insertNotFull(newroot, descriptor, pizza, descriptor->root);

        return 1;
    }
    
    //close every file 
    fclose(data);
    fclose(index);
    fclose(pizza_f);

    //if the root is not full, just inserts the element
    insertNotFull(root, descriptor, pizza, descriptor->root);

    return 1;

}

static void splits(Desc * descriptor, long pos_c, 
                                long pos_p ,int n, int pInData, int cInData, int pIsNewRoot){

    //associates the branching factor to a variable t 
    int t = descriptor->t;

    //opens the needed files
    FILE * data = openFile(descriptor->datas, "rb+");
    FILE * index = openFile(descriptor->indexes, "rb+");

    //creates a variable c which is associated to the node that is being splitted
    PTree * c;

    //creates a variable c which is associated to the node that will receive the key at the position t
    PTree * p;

    //creates a variable s to be the c's brother
    PTree * s;
    
    //reads the node p
    if(pInData){
        fseek(data, pos_p, SEEK_SET);
        p = read_node(data, t);
    }else{
        fseek(index, pos_p, SEEK_SET);
        p = read_node(index, t);
    }

    //reads the node c
    if(cInData){
        fseek(data,pos_c, SEEK_SET);
        c = read_node(data, t);
    }else{
        fseek(index, pos_c, SEEK_SET);
        c = read_node(index, t);
    }

    //create the node s
    s = create(t);

    //if c is a leaf s will be too
    s->isLeaf = c->isLeaf;

    s->brother = c->brother;

    //The new children will be in the same level as the parent was
    s->lastIndex = c->lastIndex;
    
    int j;

    //if s is not a leaf
    if(!c->isLeaf){

        /* When the node with (2*t)-1 keys splits, 1 key goes up, so...
        * ((2*t)-1)-1 = (2*t)-2 = 2*(t-1)
        * When spliting, the node is divided in half, so...
        * (2*(t-1))/2 = t-1 */
        s->activKeys = t - 1;

        //moves to s every key that is after the key t in c
        for(j = 0; j<t-1; j++){
            s->keys[j] = c->keys[j+t];
        } 

        //moves to s every childre that is after the children t in c
        for(j = 0; j<t; j++){
            s->children[j] = c->children[j+t];
        }

    // if s is a leaf    
    }else{

        //s will have an extra key because it's a leaf
        s->activKeys = t;
        //moves every key after t, including the t key(the extra key), of the node c to s
        for(j = 0; j < t; j++) {
            s->keys[j] = c->keys[j+t-1];
            s->pizzas[j] = c->pizzas[j+t-1];
        }

    }

    //The node that is being splitted will hold the keys from the first(inclusive) to the half(exclusive)
    c->activKeys = t-1;

    //opens a space after the node c in the father node to insert the node s
    for(j = p->activKeys; j >= n; j--) p->children[j+1] = p->children[j];

    //do the same thing with the keys
    for(j = p->activKeys; j >= n; j--) p->keys[j] = p->keys[j-1];

    //puts the t key in the father node
    p->keys[n-1] = c->keys[t-1];

    //update the number of active keys
    p->activKeys++;

    //saves the nodes s in archive
    //if s is a leaf
    if(s->isLeaf){

        //goes to the last position of the archive
        fseek(data, 0, SEEK_END);

        //saves the position of s in the archive
        long pos_s = ftell(data);

        //saves the node s
        save_node(data, s, t);

        //sets the brother of c to be s
        c->brother = pos_s;

        //puts s in the space that we opened 
        p->children[n] = pos_s;

    // if s is not a leaf, s is not c's brother 
    }else{
        fseek(index, 0, SEEK_END);
        long pos_s = ftell(index);
        save_node(index, s, t);
        p->children[n] = pos_s;
    }

    //overwrites the node c in archive with his new state
    if(c->isLeaf){
        fseek(data, pos_c, SEEK_SET);
        save_node(data, c, t);
    }else{
        fseek(index, pos_c, SEEK_SET);
        save_node(index, c, t);
    }

    //if the node that is being splitted is the root
    if(pIsNewRoot){

        //saves the node p in archive
        if(pInData){
            //if p is in data, saves p in the index file
            fseek(index, 0, SEEK_END);
            pos_p = ftell(index);
            save_node(index, p, t);
        }else{
            fseek(index, pos_p, SEEK_SET);
            save_node(index, p, t);
        }

        //updates the descriptor with the new root
        descriptor->root = pos_p;
        descriptor->rootInData = 0;

    //if the operation is not in the root
    }else{
        //just saves the node
        fseek(index, pos_p, SEEK_SET);
        save_node(index, p, t);
    }


    //free every node
    freePTree(c);
    freePTree(p);
    freePTree(s);

    //closes the archive
    fclose(data);
    fclose(index);

}


static void insertNotFull(PTree * pt, Desc * desc, Pizza * p, long pos_pt) {

    //Assigns i to last index of keys
    int i = pt->activKeys-1;

    //If the node we're trying to insert is a leaf
    if(pt->isLeaf) {
        
        //Searches the index of the last element smaller than p->code
        while((i>=0) && (p->code < pt->keys[i])){

            //Pushes foward the elements
            pt->keys[i+1] = pt->keys[i];
            pt->pizzas[i+1] = pt->pizzas[i];
            i--;
        }

        //Assigns the code to the right position
        pt->keys[i+1] = p->code;

        //The new key that we just inserted
        pt->activKeys++;

        //Opens pizza file
        FILE * pizzaFile = openFile(desc->pizzas, "rb");

        //Sets the cursor to the last pizza inserted in the pizzas file
        fseek(pizzaFile, (-1)*sizeOfPizza(), SEEK_END);

        //Assigns the adress of the just inserted pizza
        long pizzaAdress = ftell(pizzaFile);

        //Assigns the adress to the pizzas array
        pt->pizzas[i+1] = pizzaAdress;

        //Closes the pizza file
        fclose(pizzaFile);

        //Opens the data file
        FILE * dataFile = openFile(desc->datas, "rb+");

        //Writes the updated PTree node in the data file
        fseek(dataFile, pos_pt, SEEK_SET);
        save_node(dataFile, pt, desc->t);

        //Closes the data file
        fclose(dataFile);

        return;
    }

    //Assigns t to the ramification factor
    int t = desc->t;

    //If the node isn't a leaf
    //Searches the index of the last key smaller than p->code
    while((i>=0) && (p->code < pt->keys[i])) i--;

    /*Assigns i to the first child with bigger keys than
    * the last key with smaller keys than p->code*/
    i++;

    //Initializes the child to operate;
    PTree * child;

    //If the child of PTree node is in the data file
    if(pt->lastIndex) {
        FILE * dataFile = openFile(desc->datas, "rb");
        fseek(dataFile, pt->children[i], SEEK_SET);
        child = read_node(dataFile, t);
        fclose(dataFile);
    }

    //If the child of PTree node is in the index file
    else {
        FILE * indexFile = openFile(desc->indexes, "rb");
        fseek(indexFile, pt->children[i], SEEK_SET);
        child = read_node(indexFile, t);
        fclose(indexFile);
    }

    //If the child where p->code will be inserted is full
    if(child->activKeys == ((2*t)-1)){

        freePTree(child);

        //splits the child node updating the current node
        splits(desc, pt->children[i], pos_pt,(i+1),0,pt->lastIndex, 0);

        //releases the non-updated pt
        freePTree(pt);

        //reads the updated pt
        FILE * indexfile = openFile(desc->indexes, "rb");
        fseek(indexfile, pos_pt, SEEK_SET);
        pt = read_node(indexfile, t);
        fclose(indexfile);
        
        //If the key that came up is smaller than p->code
        if(p->code > pt->keys[i]){
            i++;
        }

        //reloads the child with the updated i;
        if(pt->lastIndex) {
            FILE * dataFile = openFile(desc->datas, "rb");
            fseek(dataFile, pt->children[i], SEEK_SET);
            child = read_node(dataFile, t);
            fclose(dataFile);
        }else {
            FILE * indexFile = openFile(desc->indexes, "rb");
            fseek(indexFile, pt->children[i], SEEK_SET);
            child = read_node(indexFile, t);
            fclose(indexFile);
        }
    
    }

    //Inserts in the child "i", or it's grandchild
    insertNotFull(child, desc, p, pt->children[i]);
    
    #if DEBUG
    int l;
    if(pos_pt == desc->root){
        printf("\nRaiz Após a inserção:\n");
        printf("ativas: %d folha: %d ultimo: %d\n", pt->activKeys, pt->isLeaf, pt->lastIndex);
        for(l = 0; l<((2*t)-1); l++) printf("chave: %d ", pt->keys[l]);
        printf("\n");
        for(l = 0; l<(2*t);l++) printf("filhos:%ld ", pt->children[l]);
        printf("\n");
        for(l = 0; l<((2*t)-1); l++) printf("pizzas: %ld ", pt->pizzas[l]);
        printf("irmao: %ld\n", pt->brother);
    }
    #endif
    // frees the node that is not needed anymore
    freePTree(pt);

    return;
}

FILE * openFile(const char * file, const char * type) {
    //Tries to open file
    FILE * f = fopen(file, type);
    //If an error occurred
    if(!f) {
        errorInFileOpening();
        exit (1);
    }

    return f;
}

Desc * operateDB(char * entry, Desc * desc) {
    //Boolean for operation control
    int op;

    //Opens entry file
    FILE * in = openFile(entry, "rb");
    fseek(in, 0L, SEEK_SET);

    //Initializes variable to read from the entry
    Pizza * p;

    do {
        prints(desc);

        //Constrols input from the user
        op = whichOption();
        if(!op) break;

        if(op == 1) {
            //Reads the pizza from the file
            p = readPizza(in);

            //Inserts in the Data Base
            inserts(desc, p);
        }

        else if(op == 2) {
            char category [20];
            readCategory(category);
            searchCategory(desc, category);
            printSearch(desc, category);
        }
        
        else if (op == 3) {
            int code = readCode();
            long found = search_pizza(desc, code);
            if(found != LONG_MAX)showPizza(found, desc);
            else pizzaNotFound();
        }

        else if (op == 4) {
            int code = readCode();
            long found = search_pizza(desc, code);
            if(found != LONG_MAX)editPizza(desc, found);
            else pizzaNotFound();
        }

        else if (op == 5) {
            int code = readCode();
            init_remove(desc, code);
        }

        else if (op == 6) {
            char category [20];
            readCategory(category);
            searchCategory(desc, category);
            removeCategory(desc);
        }

        else if (op == 7) {
            int code = readCode();
            p = readNewPizza(code);
            inserts(desc, p);
        }

        else if(op == 8) {
            int sure = areYouSureMessage();
            if(!sure) continue;
            erasingFilesMessage();
            desc = eraseFiles(desc);
        }

        #if DEBUG
        //printf("Impressao em pizza!\n");
        //printData(desc->pizzas);
        printf("\nA RAIZ ESTA AQUI:  %lu\n", desc->root);
        printf("Impressao dos arquivos!\n");
        printFiles(desc);
        #endif

    }while(op);

    //Closes the entry file
    fclose(in);
    return desc;
}

void freePTree(PTree * pt) {
    free(pt->keys);
    free(pt->children);
    free(pt->pizzas);
    free(pt);
}

#if DEBUG
static void printFiles(Desc * desc) {
    FILE * index = openFile(desc->indexes, "rb");
    fseek(index, 0L, SEEK_END);
    long end = ftell(index);
    fseek(index, 0L, SEEK_SET);
    long current = ftell(index);

    PTree * novo;
    int i, t=desc->t;
    while(current < end) {
        novo = read_node(index, t);
        printf("\nREADING NODE IN INDEX FILE...\n");
        printf("\nNó atual: %ld\n", current/88l);
        printf("ativas: %d folha: %d ultimo: %d\n", novo->activKeys, novo->isLeaf, novo->lastIndex);
        for(i = 0; i<((2*t)-1); i++) printf("chave: %d ", novo->keys[i]);
        printf("\n");
        for(i = 0; i<(2*t);i++) printf("filhos:%ld ", (novo->children[i]/88l));
        printf("\n");
        for(i = 0; i<((2*t)-1); i++) printf("pizzas: %ld ", novo->pizzas[i]);
        printf("irmao: %ld\n", (novo->brother/88l));
        current = ftell(index);
        //freePTree(novo);
    }
    fclose(index);
    
    FILE * data = openFile(desc->datas, "rb");
    fseek(data, 0L, SEEK_END);
    end = ftell(data);
    fseek(data, 0L, SEEK_SET);
    current = ftell(data);
    while(current < end) {
        novo = read_node(data, t);
        printf("\nREADING NODE IN DATA FILE...\n");
        printf("\nNó atual: %ld\n", current/88l);
        printf("ativas: %d folha: %d ultimo: %d\n", novo->activKeys, novo->isLeaf, novo->lastIndex);
        for(i = 0; i<((2*t)-1); i++) printf("chave: %d ", novo->keys[i]);
        printf("\n");
        for(i = 0; i<(2*t);i++) printf("filhos:%ld ", (novo->children[i]/88l));
        printf("\n");
        for(i = 0; i<((2*t)-1); i++) printf("pizzas: %ld ", novo->pizzas[i]);
        printf("irmao: %ld\n", (novo->brother/88l));
        current = ftell(data);
        //freePTree(novo);
    }
    fclose(data);

}
#endif

int sizeOfPTree(int t) { //if t=2, then...
    return sizeof(int) + // 4
    sizeof(int) + // 4
    sizeof(int) + // 4
    (sizeof(int)*(t*2)-1) + // 12
    sizeof(long)*2*t + // 32
    (sizeof(long)*(t*2)-1) + //24
    sizeof(long); //8
    //Retorns 88
}

void searchCategory(Desc * desc, char * category) {

    messageSearchCategory(category);
    //Opens the data file
    FILE * data = openFile(desc->datas, "rb");
    long c = smallerNode(desc);
    fseek(data, c, SEEK_SET);

    //Reads the node from the data file
    PTree * toSearch;

    //Opens the pizzas file
    FILE * pizzas = openFile(desc->pizzas, "rb");

    //Initializes variable to read from the pizza file
    Pizza * p;

    FILE * busca = openFile(desc->busca, "wb");

    //Iterates through the TLSE in the end of the tree
    for(long b = c; b != LONG_MAX; b = toSearch->brother) {

        fseek(data, b, SEEK_SET);

        toSearch = read_node(data, desc->t);
        
        for(int i=0; i<toSearch->activKeys; i++) {
            //Seek the position in the pizza's file
            fseek(pizzas, toSearch->pizzas[i], SEEK_SET);

            //Read the pizza and prints it
            p = readPizza(pizzas);

            if(!strcmp(p->description, category))savePizza(p, busca);
        }

    }

    //Closes the opened files and frees the nodes
    fclose(data);
    fclose(pizzas);
    fclose(busca);
    freePTree(toSearch);
    free(p);
    return;

}

long search_pizza(Desc * descriptor, int code){
    messageSearchPizza(code);

    //defines the not found value to LONG_MAX
    long notfound = LONG_MAX;

    //if there's only one node
    if(descriptor->rootInData){

        #if DEBUG
        printf("\n\nrootindata\n\n");
        #endif

        //opens the data file
        FILE * datafile = openFile(descriptor->datas, "rb");
        fseek(datafile, descriptor->root, SEEK_SET);

        //reads the node
        PTree * node = read_node(datafile, descriptor->t);

        //closes the file that is not needed anymore
        fclose(datafile);
        int i;

        //searches for the pizza code in the node
        for(i = 0; i<node->activKeys; i++){

            //if the node was found, returns its position in the file
            if(node->keys[i] == code){
                long pos = node->pizzas[i];
                freePTree(node);
                return pos;
            } 
        }

        //if the pizza was not found releases the node and return the notfound code;
        freePTree(node);
        return notfound;
    }
    //if root is an index
    //opens the index file and reads the root
    FILE * indexfile = openFile(descriptor->indexes, "rb");
    fseek(indexfile, descriptor->root, SEEK_SET);
    PTree * node = read_node(indexfile, descriptor->t);

    //printf("Descriptor->root: %ld", descriptor->root);

    int i;
    
    #if DEBUG
    int l;
    if(node->lastIndex){
        printf("\nLendo a raiz\n");
        printf("ativas: %d folha: %d ultimo: %d\n", node->activKeys, node->isLeaf, node->lastIndex);
        for(l = 0; l<((2*descriptor->t)-1); l++) printf("chave: %d ", node->keys[l]);
        printf("\n");
        for(l = 0; l<(2*descriptor->t);l++) printf("filhos:%ld ", node->children[l]);
        printf("\n");
        for(l = 0; l<((2*descriptor->t)-1); l++) printf("pizzas: %ld ", node->pizzas[l]);
        printf("irmao: %ld\n", node->brother);
    }
    #endif

    //searches for the node that holds the pizza's informations
    while(!node->isLeaf){

        i = 0;

        //puts i in the position where the pizza could be
        while((i < node->activKeys) && (code >= node->keys[i])) i++;

        #if DEBUG
        int l;
        printf("\nLendo o caminho\n");
        printf("\ni: %d\n", i);
        printf("ativas: %d folha: %d ultimo: %d\n", node->activKeys, node->isLeaf, node->lastIndex);
        for(l = 0; l<((2*descriptor->t)-1); l++) printf("chave: %d ", node->keys[l]);
        printf("\n");
        for(l = 0; l<(2*descriptor->t);l++) printf("filhos:%ld ", node->children[l]);
        printf("\n");
        for(l = 0; l<((2*descriptor->t)-1); l++) printf("pizzas: %ld ", node->pizzas[l]);
        printf("irmao: %ld\n", node->brother);
        #endif

        //saves the position and releases the node
        long pos = node->children[i];
        int lasti = node->lastIndex;

        freePTree(node);

        //if the next node is a leaf
        if(lasti){

            //close the index file
            fclose(indexfile);

            //opens the datafile and reads the node 
            FILE * datafile = openFile(descriptor->datas, "rb");
            fseek(datafile, pos, SEEK_SET);
            node = read_node(datafile, descriptor->t);

            #if DEBUG
            int l;
            
            printf("\nLendo onde será inserido/removido\n");
            printf("ativas: %d folha: %d ultimo: %d\n", node->activKeys, node->isLeaf, node->lastIndex);
            for(l = 0; l<((2*descriptor->t)-1); l++) printf("chave: %d ", node->keys[l]);
            printf("\n");
            for(l = 0; l<(2*descriptor->t);l++) printf("filhos:%ld ", node->children[l]);
            printf("\n");
            for(l = 0; l<((2*descriptor->t)-1); l++) printf("pizzas: %ld ", node->pizzas[l]);
            printf("irmao: %ld\n", node->brother);
            
            #endif

            //closes the datafile
            //now the node is a leaf
            fclose(datafile);

        //if the the next node is not a leaf
        }else{

            //reads the next node
            fseek(indexfile, pos, SEEK_SET);
            node = read_node(indexfile, descriptor->t);
        }
    }
    //now the node where the pizza could be was found

    //searches for the pizza in the node
    for(i = 0; i<node->activKeys; i++){
        //if the pizza was found returns the position of the pizza in the file
        if(node->keys[i] == code){
            long pos = node->pizzas[i];
            freePTree(node);
            return pos;
        } 
    }

    //if the pizza was not found returns the not found code
    freePTree(node);
    return notfound;

}

void editPizza(Desc * desc, long pos_pizza) {
    FILE * pizzas = openFile(desc->pizzas, "rb+");
    fseek(pizzas, pos_pizza, SEEK_SET);
    Pizza * get_code = readPizza(pizzas);
    fseek(pizzas, pos_pizza, SEEK_SET);

    Pizza * pizza = readNewPizza(get_code->code);

    messageEditPizza();
    savePizza(pizza, pizzas);
    fclose(pizzas);
}

//function that calls the removes function for the first time
void init_remove(Desc * descriptor, int code){

    // if the pizza that we are trying to remove is not in the tree, just returns
    if(search_pizza(descriptor, code) == LONG_MAX) {
        pizzaNotFound();
        return;
    }

    messageRemovePizza(code);

    //creates a node to receive the root
    PTree * root;

    //discovers in witch file the root is and read it
    if(descriptor->rootInData){
        FILE * datafile = openFile(descriptor->datas, "rb");
        fseek(datafile, descriptor->root, SEEK_SET);
        root = read_node(datafile, descriptor->t);
        fclose(datafile);
    }else{
        FILE * indexfile = openFile(descriptor->indexes, "rb");
        fseek(indexfile, descriptor->root, SEEK_SET);
        root = read_node(indexfile, descriptor->t);
        fclose(indexfile);
    }

    //calls the function that is going to remove the pizza
    removes(descriptor, root, code, descriptor->root);

    //frees the node 
    freePTree(root);

    //reads the new root in order to discover if it still exists
    if(!descriptor->rootInData){
        FILE * indexfile = openFile(descriptor->indexes, "rb");
        fseek(indexfile, descriptor->root, SEEK_SET);
        root = read_node(indexfile, descriptor->t);
        fclose(indexfile);

        //if root has not any keys, means that it was merged with the children 0
        if(root->activKeys == 0){

            //puts the children 0 as the new root
            descriptor->root = root->children[0];
            if(root->lastIndex){
                descriptor->rootInData = 1;
            }
        }

        //releases the root
        freePTree(root);
    }
    
}

void removes(Desc * descriptor, PTree * node, int code, long pos_node){


    int i = 0;

    //find in witch position the node is or should be
    while((i < node->activKeys) && (node->keys[i] < code)) i++;

    //if the node is a leaf
    if(node->isLeaf){

        //move every key in order to overwrite the key we want to remove
        for(; i< (node->activKeys-1); i++){
            node->keys[i] = node->keys[i+1];
            node->pizzas[i] = node->pizzas[i+1];
        }

        //updates the number of active keys in the node
        node->activKeys--;

        if(descriptor->rootInData && (pos_node == descriptor->root) && node->activKeys == 0){
            FILE * datafile = openFile(descriptor->datas, "wb");
            fclose(datafile);
            FILE * indexfile = openFile(descriptor->indexes, "wb");
            fclose(indexfile);
            FILE * pizzafile = openFile(descriptor->pizzas, "wb");
            fclose(pizzafile);
            descriptor->root = 0l;
            return;
        }

        //saves the node in the file again and returns
        FILE * datafile = openFile(descriptor->datas, "rb+");
        fseek(datafile, pos_node, SEEK_SET);
        save_node(datafile, node, descriptor->t);
        fclose(datafile);
        return;
    }

    //if the node is not a leaf
    //updates the value of i to witch children the node could be
    if((node->keys[i] == code) && (i != node->activKeys))i++;

    //creates a boolean that will tell witch children we will take to balance
    int case1 = 0;

    //creates two nodes, children1 to hold the children where the key could be and children two
    //witch is the children that will help us to balance the tree
    PTree * children1, * children2, *children3;

    //creates two variables to save the positions of childrens 1 and 2
    long pos_chil1, pos_chil2, pos_chil3;

    //verifies from witch file we should read the children
    if(node->lastIndex){

        //opens the file and reads the children
        FILE * datafile = openFile(descriptor->datas, "rb");
        fseek(datafile, node->children[i], SEEK_SET);
        children1 = read_node(datafile, descriptor->t);
        fclose(datafile);

        //calls the recursion to the children
        removes(descriptor, children1, code, node->children[i]);

        //frees the children that is not updated
        freePTree(children1);

        //opens the file and reads the updated children
        datafile = openFile(descriptor->datas, "rb");
        fseek(datafile, node->children[i], SEEK_SET);
        pos_chil1 = ftell(datafile);
        children1 = read_node(datafile, descriptor->t);

        //if the removal caused a imbalance in the tree reads the immediate brother that will help
        if(children1->activKeys < (descriptor->t-1)){

            // if i is the last children, we must choose the children that comes before
            if(i == node->activKeys){

                //read the new children
                fseek(datafile, node->children[i-1], SEEK_SET);
                pos_chil2 = ftell(datafile);
                children2 = read_node(datafile, descriptor->t);

                //defines that this is the case 1
                case1 = 1;
            }else if(i == 0){
                //read the new children
                fseek(datafile, node->children[i+1], SEEK_SET);
                pos_chil2 = ftell(datafile);
                children2 = read_node(datafile, descriptor->t);
            }else{
                fseek(datafile, node->children[i+1], SEEK_SET);
                pos_chil2 = ftell(datafile);
                children2 = read_node(datafile, descriptor->t);
                if(children2->activKeys == descriptor->t-1){
                    fseek(datafile, node->children[i-1], SEEK_SET);
                    pos_chil3 = ftell(datafile);
                    children3 = read_node(datafile, descriptor->t);
                    if(children3->activKeys >= descriptor->t){
                        freePTree(children2);
                        children2 = children3;
                        pos_chil2 = pos_chil3;
                        case1 = 1;
                    }else{
                        freePTree(children3);
                    }
                }
            }
        }else{

            //if the removal did not caused a imbalance, just returns;
            fclose(datafile);
            return;
        }
        //closes the archive that we don't need anymore
        fclose(datafile);
    }else{

        //do the same things that we would do with the datafile
        //reads the children
        FILE * indexfile = openFile(descriptor->indexes, "rb");
        fseek(indexfile, node->children[i], SEEK_SET);
        children1 = read_node(indexfile, descriptor->t);
        fclose(indexfile);

        //call recursion
        removes(descriptor, children1, code, node->children[i]);
        freePTree(children1);

        //updates the value
        indexfile = openFile(descriptor->indexes, "rb");
        fseek(indexfile, node->children[i], SEEK_SET);
        pos_chil1 = ftell(indexfile);
        children1 = read_node(indexfile, descriptor->t);

        //decides the children that will help
        if(children1->activKeys < (descriptor->t-1)){
            
            #if DEBUG
            printf("\ni: %d node->activKeys: %d\n", i, node->activKeys);
            #endif
            if(i == node->activKeys){
                fseek(indexfile, node->children[i-1], SEEK_SET);
                pos_chil2 = ftell(indexfile);
                children2 = read_node(indexfile, descriptor->t);
                case1 = 1;
            }else if(i == 0){
                fseek(indexfile, node->children[i+1], SEEK_SET);
                pos_chil2 = ftell(indexfile);
                children2 = read_node(indexfile, descriptor->t);
            }else{
                fseek(indexfile, node->children[i+1], SEEK_SET);
                pos_chil2 = ftell(indexfile);
                children2 = read_node(indexfile, descriptor->t);
                if(children2->activKeys == descriptor->t-1){
                    fseek(indexfile, node->children[i-1], SEEK_SET);
                    pos_chil3 = ftell(indexfile);
                    children3 = read_node(indexfile, descriptor->t);
                    if(children3->activKeys >= descriptor->t){
                        freePTree(children2);
                        children2 = children3;
                        pos_chil2 = pos_chil3;
                        case1 = 1;
                    }else{
                        freePTree(children3);
                    }
                }
            }
        }else{
            fclose(indexfile);
            return;
        }
        //closes the file
        fclose(indexfile);
    }

    //creates a iteration variable j, a boolean that tell us if we did solve the problem with the 
    //first technique and two booleans that tell us if we killed one of the childrens in the process 
    int j, solved = 0, killchil1 = 0, killchil2 = 0;

    //first tries to solve the problem using redistribution
    //if the operation involves just indexes, it's the same as B tree
    //if the operation involves leafs and indexes, we should deal with it in a different way
    if(children1->isLeaf){

        //if the brother have sufficient keys to gently donate to children1
        if((children2->activKeys >= descriptor->t) && case1){//Caso 3A1 B+

            #if DEBUG
            printf("Caso3A1 B+\n");
            #endif

            //opens a space in children1 to the new key
            for(j = children1->activKeys; j>0; j--){
                children1->keys[j] = children1->keys[j-1];
                children1->pizzas[j] = children1->pizzas[j-1];
            }

            //puts the last key of children2 as the first key of children1
            children1->keys[0] = children2->keys[children2->activKeys-1];
            children1->pizzas[0] = children2->pizzas[children2->activKeys-1];

            //updates the value of activkeys;
            children1->activKeys++;
            children2->activKeys--;

            //give to the father one of the children's keys
            node->keys[i-1] = children1->keys[0];

            //now the problem is solved
            solved = 1;

        //if not case 1 
        }else if(children2->activKeys >= descriptor->t){//Caso 3A2 B+

            #if DEBUG
            printf("Caso3A2 B+\n");
            #endif

            //children 1 receives the first key children 2
            children1->keys[children1->activKeys] = children2->keys[0];
            children1->pizzas[children1->activKeys] = children2->pizzas[0];

            //overwrites the key we just moved
            for(j = 0; j<children2->activKeys-1; j++){
                children2->keys[j] = children2->keys[j+1];
                children2->pizzas[j] = children2->pizzas[j+1];
            }

            //updates the value of activKeys
            children1->activKeys++;
            children2->activKeys--;

            //gives a key to the father node
            node->keys[i] = children2->keys[0];

            //now the imbalace is solved
            solved = 1;
        }
    
    //if the childrens are not leafs, applies the B tree algorithm
    }else{

        //if children2 have sufficient keys to donate one and we are in case 1
        if((children2->activKeys >= descriptor->t) && case1){//Caso 3A1 B

            #if DEBUG
            printf("Caso3A1 B\n");
            #endif

            //opens a space in children 1 to receive the father's key
            for(j = children1->activKeys; j>0; j--){
                children1->keys[j] = children1->keys[j-1];
            }
            for(j = children1->activKeys+1; j>0; j--){
                children1->children[j] = children1->children[j-1];
            }

            //give to children1 a key from the father
            children1->keys[0] = node->keys[i-1];

            //gives to the father a key from the brother
            node->keys[i-1] = children2->keys[children2->activKeys-1];

            //children1 receive the children2's children referent to teh key we just moved
            children1->children[0] = children2->children[children2->activKeys];

            //update the values of children1 and 2
            children1->activKeys++;
            children2->activKeys--;

            if(children1->lastIndex){
               FILE * data = openFile(descriptor->datas, "rb+");
               fseek(data, children1->children[0], SEEK_SET);
               PTree * temp = read_node(data, descriptor->t);
               temp->brother = children1->children[1];
               fseek(data, children1->children[0], SEEK_SET);
               save_node(data, temp, descriptor->t);
               fclose(data);
               freePTree(temp);
            } 

            //now the problem is solved
            solved = 1;
        
        //if not case 1
        }else if((children2->activKeys >= descriptor->t)){//Caso 3A2 B

            #if DEBUG
            printf("Caso3A2 B\n");
            #endif

            //children1 receives a key from the father
           children1->keys[children1->activKeys] = node->keys[i];

           //update the number of children1's keys
           children1->activKeys++;

           //the father receives a key from children2
           node->keys[i] = children2->keys[0];

           //overwrites the position of the key we just moved
           for(j = 0; j<children2->activKeys-1; j++){
               children2->keys[j] = children2->keys[j+1];
           }

           //reorganizes the children
           children1->children[children1->activKeys] = children2->children[0];

           for(j = 0; j<children2->activKeys; j++){
               children2->children[j] = children2->children[j+1];
           }

           //updates the number of keys of children2
           children2->activKeys--;

           if(children1->lastIndex){
               FILE * data = openFile(descriptor->datas, "rb+");
               fseek(data, children1->children[children1->activKeys-1], SEEK_SET);
               PTree * temp = read_node(data, descriptor->t);
               temp->brother = children1->children[children1->activKeys];
               fseek(data, children1->children[children1->activKeys-1], SEEK_SET);
               save_node(data, temp, descriptor->t);
               fclose(data);
               freePTree(temp);
           } 

           //now the problem is solved
           solved = 1;
        }
    }

    //if the problem could not be solved with redistribution, we must concatanate children1 and 
    //children2
    if(!solved){

        //again, it's different if we are dealing with indexes and data or just indexes
        //if we are dealing with indexes and data
        if(children1->isLeaf){

            //if case 1
            if(case1){//Caso 3B1 B+

                #if DEBUG
                printf("Caso3B1 B+\n");
                #endif

                //puts the children1's keys in children2
                for(j = 0; j<children1->activKeys; j++){
                    children2->keys[children2->activKeys] = children1->keys[j];
                    children2->pizzas[children2->activKeys] = children1->pizzas[j];
                    children2->activKeys++;
                }

                //overwrites the key that was between the childrens
                for(j = i-1; j<node->activKeys-1; j++){
                    node->keys[j] = node->keys[j+1];
                }

                //organizes the childrens
                for(j = i; j<node->activKeys; j++){
                    node->children[j] = node->children[j+1];
                }

                //update the number of keys of the node
                node->activKeys--;

                //remake the link of the linkedlist
                children2->brother = children1->brother;

                //we killed children1 in the process D:
                killchil1 = 1;
            
            //if not case 1 children1 and children2 change roles
            }else{//Caso 3B2 B+

                #if DEBUG
                printf("Caso3B2 B+\n");
                #endif

                //puts children2's keys into children1
                for(j = 0; j<children2->activKeys; j++){
                    children1->keys[children1->activKeys] = children2->keys[j];
                    children1->pizzas[children1->activKeys] = children2->pizzas[j];
                    children1->activKeys++;
                }

                //overwrites the key that was between the childrens
                for(j = i; j<node->activKeys-1; j++){
                    node->keys[j] = node->keys[j+1];
                }

                //organizes the childrens
                for(j = i+1; j<node->activKeys; j++){
                    node->children[j] = node->children[j+1];
                }

                //remake the linkedlist
                children1->brother = children2->brother;

                //updates the number of keys of the father
                node->activKeys--;

                //and now children2 dies :/
                killchil2 = 1;
            }
        
        //if we are dealing with indexes only, it's the same algorithm as B tree
        }else{

            //if it's case 1
            if(case1){//Caso 3B1 B

                #if DEBUG
                printf("Caso3B1 B\n");
                #endif

                //children2 receives a key from the father
                children2->keys[children2->activKeys] = node->keys[i-1];
 
                //update the number of keys 
                children2->activKeys++;

                //children2 receives also the children1's keys
                for(j = 0; j<=children1->activKeys; j++){
                    children2->children[children2->activKeys+j] = children1->children[j]; 
                }

                if(children2->lastIndex){
                    FILE * data = openFile(descriptor->datas, "rb+");
                    fseek(data, children2->children[children2->activKeys-1], SEEK_SET);
                    PTree * temp = read_node(data, descriptor->t);
                    temp->brother = children2->children[children2->activKeys];
                    fseek(data, children2->children[children2->activKeys-1], SEEK_SET);
                    save_node(data, temp, descriptor->t);
                    fclose(data);
                    freePTree(temp);
                }

                //reorganizes the keys
                for(j = 0; j<children1->activKeys; j++){
                    children2->keys[children2->activKeys] = children1->keys[j];
                    children2->activKeys++; 
                }

                for(j = i-1; j<node->activKeys-1; j++){
                    node->keys[j] = node->keys[j+1];
                }

                for(j = i; j<node->activKeys; j++){
                    node->children[j] = node->children[j+1];
                }
                
                //update the number of keys of the father
                node->activKeys--;

                //we killed children1 in the process D:
                killchil1 = 1;

            // if it's not case 1 children1 and children2 switch roles
            }else{//Caso 3B2 B

                #if DEBUG
                printf("Caso3B2 B\n");
                #endif

                //children1 receives a key from the father
                children1->keys[children1->activKeys] = node->keys[i];

                //updates the number of keys
                children1->activKeys++;

                //children1 receives childre2's keys and children
                for(j = 0; j<=children2->activKeys; j++){
                    children1->children[children1->activKeys+j] = children2->children[j]; 
                }

                if(children1->lastIndex){
                    FILE * data = openFile(descriptor->datas, "rb+");
                    fseek(data, children1->children[children1->activKeys-1], SEEK_SET);
                    PTree * temp = read_node(data, descriptor->t);
                    temp->brother = children1->children[children1->activKeys];
                    fseek(data, children1->children[children1->activKeys-1], SEEK_SET);
                    save_node(data, temp, descriptor->t);
                    fclose(data);
                    freePTree(temp);
                }

                for(j = 0; j<children2->activKeys; j++){
                    children1->keys[children1->activKeys] = children2->keys[j];
                    children1->activKeys++; 
                }

                for(j = i; j<node->activKeys-1; j++){
                    node->keys[j] = node->keys[j+1];
                }

                for(j = i+1; j<node->activKeys; j++){
                    node->children[j] = node->children[j+1];
                }

                //update the number of keys of the father
                node->activKeys--;

                //and we killed children2 in the process :/
                killchil2 = 1;
            }
        }
    }

    //now that the problem is solved, we should save the result in the archive

    //opens the file
    FILE * datafile = openFile(descriptor->datas, "rb+");
    FILE * indexfile = openFile(descriptor->indexes, "rb+");

    //finds out where to write
    if(node->lastIndex){

        //if we killed children 1, saves just children 2
        if(killchil1){
            fseek(datafile, pos_chil2, SEEK_SET);
            save_node(datafile, children2, descriptor->t);

        //if we killed children 2, just saves children 1
        }else if(killchil2){
            fseek(datafile, pos_chil1, SEEK_SET);
            save_node(datafile, children1, descriptor->t);

        //if we did not kill anyone, saves both
        }else{
            fseek(datafile, pos_chil2, SEEK_SET);
            save_node(datafile, children2, descriptor->t);
            fseek(datafile, pos_chil1, SEEK_SET);
            save_node(datafile, children1, descriptor->t);
        }

        //saves the father in the indexfile
        fseek(indexfile, pos_node, SEEK_SET);
        save_node(indexfile, node, descriptor->t);
    }else{

        //do exactly the same thing that we did before but only with indexes
        if(killchil1){
            fseek(indexfile, pos_chil2, SEEK_SET);
            save_node(indexfile, children2, descriptor->t);
        }else if(killchil2){
            fseek(indexfile, pos_chil1, SEEK_SET);
            save_node(indexfile, children1, descriptor->t);
        }else{
            fseek(indexfile, pos_chil2, SEEK_SET);
            save_node(indexfile, children2, descriptor->t);
            fseek(indexfile, pos_chil1, SEEK_SET);
            save_node(indexfile, children1, descriptor->t);
        }
        fseek(indexfile, pos_node, SEEK_SET);
        save_node(indexfile, node, descriptor->t);
    }

    //releases every node and closes every file
    //freePTree(children1);
    freePTree(children2);
    fclose(datafile);
    fclose(indexfile);

}

void removeCategory ( Desc * desc) {
    //Opens the search file
    FILE * busca = openFile(desc->busca, "rb");
    
    //Assigns end of file cursor and the iteration cursor
    fseek(busca, 0L, SEEK_END);
    long end = ftell(busca);
    fseek(busca, 0L, SEEK_SET);
    long current = ftell(busca);

    //Initializes variable to read from the pizza file
    Pizza * p;

    while(current  < end) {
        //Read the pizza
        p = readPizza(busca);
        printf("\n");
        printPizza(p);
        init_remove(desc, p->code);

        //Updates the current cursor
        current = ftell(busca);
    }

    //Closes the file and frees the pizza
    fclose(busca);
    free(p);

    return;
}

long smallerNode(Desc * desc) {
    if(desc->rootInData) return desc->root;

    FILE * index = openFile(desc->indexes, "rb");
    fseek(index, desc->root, SEEK_SET);

    long c=ftell(index);

    PTree * iterator = read_node(index, desc->t);

    for(; c != LONG_MAX; c=iterator->children[0]) {
        if(iterator->lastIndex) {
            fclose(index);
            c = iterator->children[0];
            freePTree(iterator);
            return c;
        }
        fseek(index, iterator->children[0], SEEK_SET);

        iterator = read_node(index, desc->t);

        #if DEBUG
        printf("C: %lu\n", c);
        #endif
    }

    fclose(index);
    freePTree(iterator);

    return c;
}

Desc * getDescriptor() {
    FILE * descriptor;
    Desc * desc;
    int t;

    if(!(descriptor = fopen("descriptor.dat", "rb"))) {
        descriptor = fopen("descriptor.dat", "wb");
        t = readFactor();
        desc = createDescriptor(t);
        fclose(descriptor);
        return desc;
    }

    fclose(descriptor);
    t = readBranchFactor();
    desc = readDescriptor(t);
    return desc;
}

Desc * readDescriptor(int t) {
    FILE * descriptor = openFile("descriptor.dat", "rb");
    Desc * desc = createDescriptor(t);
    int i;
    for(i=0; i<12; i++){
        #if DEBUG
        printf("%c", desc->indexes[i]);
        #endif
        fread(&desc->indexes[i], sizeof(char), 1, descriptor);
    }
    #if DEBUG
    printf("\n");
    #endif
    for(i=0; i<10; i++)fread(&desc->datas[i], sizeof(char), 1, descriptor);
    for(i=0; i<11; i++)fread(&desc->pizzas[i], sizeof(char), 1, descriptor);
    for(i=0; i<10; i++)fread(&desc->busca[i], sizeof(char), 1, descriptor);
    fread(&desc->rootInData, sizeof(int), 1, descriptor);
    fread(&desc->root, sizeof(long), 1, descriptor);
    fread(&desc->t, sizeof(int), 1, descriptor);
    fclose(descriptor);
    return desc;
}

void writeDescriptor(Desc * desc) {
    FILE * descriptor = openFile("descriptor.dat", "wb");
    int i;
    for(i=0; i<12; i++){
        #if DEBUG
        printf("%c", desc->indexes[i]);
        #endif
        fwrite(&desc->indexes[i], sizeof(char), 1, descriptor);
    }
    #if DEBUG
    printf("\n");
    #endif
    for(i=0; i<10; i++)fwrite(&desc->datas[i], sizeof(char), 1, descriptor);
    for(i=0; i<11; i++)fwrite(&desc->pizzas[i], sizeof(char), 1, descriptor);
    for(i=0; i<10; i++)fwrite(&desc->busca[i], sizeof(char), 1, descriptor);
    fwrite(&desc->rootInData, sizeof(int), 1, descriptor);
    fwrite(&desc->root, sizeof(long), 1, descriptor);
    fwrite(&desc->t, sizeof(int), 1, descriptor);
    fclose(descriptor);
}

int realSizeOfDesc() {
    return sizeof(int) + sizeof(long) + //t + root
    sizeof(int) + sizeof(char)*12 + //rootInData + indexes
    sizeof(char)*10 + sizeof(char)*11 + //datas + pizzas
    sizeof(char) * 10; //busca
}

int readBranchFactor() {
    FILE * descriptor = openFile("descriptor.dat", "rb");
    int a = fseek(descriptor, realSizeOfDesc()-sizeof(int), SEEK_SET);
    int t;
    fread(&t, sizeof(int), 1, descriptor);
    showBranchingFactor(t);

    fclose(descriptor);
    return t;
}

Desc * eraseFiles(Desc * desc) {
    FILE * index = openFile(desc->indexes, "wb");
    FILE * datas = openFile(desc->datas, "wb");
    FILE * pizza = openFile(desc->pizzas, "wb");

    remove("descriptor.dat");

    fclose(index);
    fclose(datas);
    fclose(pizza);

    Desc * descriptor = createDescriptor(desc->t);
    return descriptor;
}