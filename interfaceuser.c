#include "interfaceuser.h"

void readEntry(char* entry) {
    printf("Digite o nome do arquivo de entrada: ");
    char name[20];
    scanf("%s", name);

    strcpy(entry, name);
}

void errorInFileOpening() {
    printf("Erro ao abrir o arquivo! Execucao interrompida!\n");
}

int readFactor() {
    int t;
    printf("Digite o fator de ramificacao: ");
    scanf("%d", &t);
    return t;
}

int whichOption() {
    int op;
    printf("\nDigite 1 para inserir do arquivo");
    printf(" 2 para buscar por uma categoria");
    printf(" 3 para buscar uma pizza especifica");
    printf("\n4 para alterar uma pizza");
    printf(" 5 para excluir uma pizza");
    printf(" 6 para excluir uma categoria de pizza");
    printf("\n7 para inserir uma pizza avulsa");
    printf(" 8 para apagar todos os arquivos");
    printf(" e 0 para sair: ");
    scanf("%d", &op);
    return op;
}

void printData(char * pizzas) {
    FILE * p = fopen(pizzas, "rb");
    if(!p) {
        errorInFileOpening();
        exit(1);
    }

    Pizza * pizza;
    while((pizza = readPizza(p)) != NULL) {
        printPizza(pizza);
    }
    fclose(p);
}

void printPizza(Pizza *p)
{
	printf("%d, %s (%s), R$ %.2f\n", p->code, p->name, p->description, p->price);
}

void errorInWritingData() {
    printf("Um erro ocorreu ao escrever dado no arquivo!\n");
}

void errorInReadingData() {
    printf("Um erro ocorreu ao ler dado do arquivo!\n");
}

void readCategory(char* category) {
    printf("\nDigite a categoria: ");
    char cat[20];
    scanf(" %[^\n]%*c", cat);

    strcpy(category, cat);
}

void messageSearchCategory(char * category) {
    printf("Buscando por categoria \"%s\" em banco de dados...\n", category);
}

void prints(Desc * desc) {
    //Opens the data file
    FILE * data = openFile(desc->datas, "rb");

    //If the file is empty
    fseek(data, 0L, SEEK_SET);
    long start = ftell(data);
    fseek(data, 0L, SEEK_END);
    long end = ftell(data);
    if(start==end) {
        #if DEBUG
        printf("ENTREI AQUI\n");
        #endif
        fclose(data);
        return;
    }

    long c = smallerNode(desc);
    fseek(data, c, SEEK_SET);

    //Reads the node from the data file
    PTree * toPrint;

    //Opens the pizzas file
    FILE * pizzas = openFile(desc->pizzas, "rb");

    //Initializes variable to read from the pizza file
    Pizza * p;

    printf("\nImpressao dos dados...\n");

    //Iterates through the TLSE in the end of the tree
    for(long b = c; b != LONG_MAX; b = toPrint->brother) {

        fseek(data, b, SEEK_SET);

        toPrint = read_node(data, desc->t);

        /*#if DEBUG
        printf("\ntoPrint->brother: %ld\n", toPrint->brother);
        #endif*/
        
        for(int i=0; i<toPrint->activKeys; i++) {
            //Seek the position in the pizza's file
            fseek(pizzas, toPrint->pizzas[i], SEEK_SET);

            //Read the pizza and prints it
            p = readPizza(pizzas);

            printPizza(p);
        }

    }

    //Closes the opened files and frees the nodes
    fclose(data);
    fclose(pizzas);
    freePTree(toPrint);
    free(p);
    return;
}

int readCode() {
    int code;
    printf("\nDigite o codigo da pizza: ");
    scanf("%d", &code);
    return code;
}

void pizzaNotFound() {
    printf("Nao encontramos a pizza em nosso banco de dados\n");
}

void showPizza(long p, Desc * desc) {
    FILE * pizzas = openFile(desc->pizzas, "rb");
    fseek(pizzas, p, SEEK_SET);
    Pizza *pizza = readPizza(pizzas);
    printPizza(pizza);
    fclose(pizzas);
}

void messageSearchPizza(int codigo) {
    printf("\nBuscando pizza de codigo %d...\n", codigo);
}

Pizza * readNewPizza(int code) {
    char name[50];
	char description[20];
	float price;
    printf("Digite um nome: ");
    scanf (" %[^\n]%*c", name);
    printf("Digite uma categoria: ");
    scanf (" %[^\n]%*c", description);
    printf("Digite um preco: ");
    scanf("%f", &price);
    Pizza*p = pizza(code, name, description, price);
    return p;
}

void messageEditPizza() {
    printf("Atualizando a pizza...\n");
}

void messageRemovePizza(int code) {
    printf("Removendo a pizza %d...", code);
}

void printSearch(Desc * desc, char*category) {
    printf("\nImpressao das pizzas da categoria %s...\n", category);

    //Opens the search file
    FILE * busca = openFile(desc->busca, "rb");

    //Assigns end of file cursor and the iteration cursor
    fseek(busca, 0L, SEEK_END);
    long end = ftell(busca);
    fseek(busca, 0L, SEEK_SET);
    long current = ftell(busca);

    //Initializes variable to read from the pizza file
    Pizza * p;

    //Iterates through the search file
    while(current < end) {

        //Read the pizza
        p = readPizza(busca);

        //Prints the pizza
        printPizza(p);
        
        //Updates the current cursor
        current = ftell(busca);
    }

    //Closes the opened files and frees the nodes
    fclose(busca);
    free(p);
    return;
}

void messagePizzaAlreadyExists() {
    printf("A pizza que deseja inserir ja esta em nosso banco de dados!\n");
}

void showBranchingFactor(int t) {
    printf("Ja existe uma arvore no banco de dados, e seu fator de ramificacao e: %d", t);
}

void erasingFilesMessage() {
    printf("\nApagando banco de dados...\n");
}

int areYouSureMessage() {
    int sure;
    printf("\nTem certeza que deseja apagar o banco de dados? ");
    scanf("%d", &sure);
    return sure;
}