#ifndef INTERFACEUSER
#define INTERFACEUSER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pizza.h"
#include "PTree.h"

void readEntry(char * entry);
void errorInFileOpening();
int readFactor();
int whichOption();
void printData();
void errorInWritingData();
void errorInReadingData();
void printPizza(Pizza *p);
void readCategory(char* category);
void messageSearchCategory(char * category);
void prints(Desc * desc);
int readCode();
void pizzaNotFound();
void showPizza(long p, Desc * desc);
void messageSearchPizza(int codigo);
Pizza * readNewPizza();
void messageEditPizza();
void messageRemovePizza(int code);
void printSearch(Desc * desc, char*category);
void messagePizzaAlreadyExists();
void showBranchingFactor(int t);
void erasingFilesMessage();
int areYouSureMessage();

#endif //INTERFACEUSER